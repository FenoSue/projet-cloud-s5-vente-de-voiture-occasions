import React from 'react';
import MenuComponent from '../components/MenuComponent';
import ListeAnnonceComponent from '../components/ListeAnnonceComponent';

const ListeAnnonce = () => {
    return (
        <div>
            <MenuComponent>
                <ListeAnnonceComponent />
            </MenuComponent>
        </div>
    );
};

export default ListeAnnonce;