import React from 'react';
import MenuComponent from '../components/MenuComponent';
import MessagerieComponent from '../components/MessagerieComponent';

const Messagerie = () => {

    return (
        <MenuComponent>
            <MessagerieComponent />
        </MenuComponent>
    );
};

export default Messagerie;