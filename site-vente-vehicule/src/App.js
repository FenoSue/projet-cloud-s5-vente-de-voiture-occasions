import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import ListeAnnonce from './pages/ListeAnnonce';
import Photo from './pages/Photo';
import Messagerie from './pages/Messagerie';

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/accueil" element={<Home />} />
        <Route path="/listeAnnonce" element={<ListeAnnonce />} />
        <Route path="/detailPhotos/:annonceId" element={<Photo />} />
        <Route path="/messages" element={<Messagerie />} />
      </Routes>
    </Router>
  );
};

export default App;
