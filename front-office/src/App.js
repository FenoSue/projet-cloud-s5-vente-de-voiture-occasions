import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Historique from './pages/Historique';
import Messagerie from './pages/Messagerie';
import ListeAnnonce from './pages/ListeAnnonce';
import Detail from './pages/Detail';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login/:url" element={<Login />} />
        <Route path="/historique" element={<Historique />} />
        <Route path="/messagerie" element={<Messagerie />} />
        <Route path="/conversation/:idUtilisateur" element={<Messagerie />} />
        <Route path="/listeannonce" element={<ListeAnnonce />} />
        <Route path="/detailannonce/:annonceId" element={<Detail />} />
      </Routes>
    </Router>
  );
}

export default App;
