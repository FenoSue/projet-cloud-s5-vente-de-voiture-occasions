import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/ListeCss.css';

const ListeAnnonceComponent = () => {
    const navigate = useNavigate();
    const [listeAnnonces, setListeAnnonces] = useState([]);

    useEffect(() => {
        getHistorique();
    }, []); 

    const getHistorique = async () => {
        const token = localStorage.getItem('token');
        try {
            const response = await fetch('http://localhost:8080/ListeAnnonceValiderUser', {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeAnnonces(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const favoriserAnnonce = async (idAnnonce) => {
        const token = localStorage.getItem('token');
        try {
            const response = await fetch('http://localhost:8080/FavoriserAnnonce', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify ({
                    id: idAnnonce,
                }),
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    window.location.reload();
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    }

    return (
        <div>
            <h4>Liste des annonces valider</h4>
            <hr></hr>
            <div className='contenu'>
                <div className='liste'>
                    <table className="table table-bordered custom-table">
                        <thead>
                        <tr>
                            <th>réf</th>
                            <th>Annonce</th>
                        </tr>
                        </thead>
                        <tbody>
                            {listeAnnonces.map((annonce) => (
                            <tr key={annonce.id}>
                                <td>
                                    <p>{annonce.id}</p>
                                </td>
                                <td>
                                    <p>Fait le {annonce.dateHeure}</p>
                                    <p>
                                        Description : c'est une voiture {annonce.categorie} {annonce.couleur}. {annonce.nbrPlace} {annonce.description}. 
                                        De marque {annonce.marque}, modèle {annonce.modele}. 
                                        Avec une {annonce.boiteVitesse}. Sortie en {annonce.anneeSortie}
                                    </p>
                                    <p>
                                        Carburant : {annonce.typeCarburant}
                                    </p>
                                    { annonce.favoris === 1 ? (
                                        <strong style={{color: 'blueviolet'}}>Favoris</strong>
                                    ) : annonce.favoris === -1 ? (
                                        <button className='form-control boutonfavoris' onClick={() => favoriserAnnonce(annonce.id)}>Favoriser</button>
                                    ):null }
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default ListeAnnonceComponent;