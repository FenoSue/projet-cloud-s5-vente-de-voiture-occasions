import React, { useState, useEffect } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faMessage } from '@fortawesome/free-solid-svg-icons';
import '../css/DetailCss.css';

const DetailComponent = () => {
    const navigate = useNavigate();
    const { annonceId } = useParams();
    const [detailAnnonce, setDetailAnnonce] = useState([]);

    console.log('idAnnonce : '+annonceId);

    useEffect(() => {
        getDetailAnnonce(annonceId);
    }, []); 

    const getDetailAnnonce = async (annonceId) => {
        console.log('Detail annonce ok!');
        try {
            const response = await fetch(`http://localhost:8080/DetailAnnonce?idAnnonce=${annonceId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setDetailAnnonce(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const goToMessage = async (idUtilisateur) => {
        console.log('idUtilisateur : '+idUtilisateur);
        const url = 'conversation/'+idUtilisateur;
        console.log('url : '+url);
        navigate(`/login/${url}`);
    }

    return (
        <div>
            {detailAnnonce.map((detailannonce) => (
                <div key={detailannonce.id}>
                    <h5 className='title'>{detailannonce.marque} {detailannonce.modele} {detailannonce.categorie} {detailannonce.couleur}</h5>
                    <Row>
                        <Col xs={3}>
                            <Card>
                                <Card.Body>
                                    <p>photos</p>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={2}></Col>
                        <Col xs={6}>
                            <Card>
                                <Card.Body>
                                    <p><strong>Boite de vitesse : </strong>{detailannonce.boiteVitesse}</p>
                                    <p><strong>Carburant : </strong>{detailannonce.typeCarburant}</p>
                                    <p><strong>Kilométrage : </strong>{detailannonce.kilometrage}</p>
                                    <p><strong>Année de sortie : </strong>{detailannonce.anneeSortie}</p>
                                    <p><strong>Couleur : </strong>{detailannonce.couleur}</p>
                                    <p><strong>Nombre de place : </strong>{detailannonce.nbrplace}</p>
                                    <p><strong>Prix : </strong>{detailannonce.prix}</p>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={1}></Col>
                    </Row>
                    <Card className='message' onClick={() => goToMessage(detailannonce.idUtilisateur)}>
                        <small>
                            Contacter le propriétaire
                        </small>
                            <FontAwesomeIcon className='iconmessage' icon={faMessage}/>
                    </Card>  
                </div>
            ))}
            <div>
                <Link className='btn boutonretour' style={{ color: 'white', backgroundColor: 'lightslategray', borderRadius: '20px' }} to="/">
                    <FontAwesomeIcon className='iconretour' icon={faArrowLeft} />
                    Retour
                </Link>
            </div>
        </div>
    );
};

export default DetailComponent;