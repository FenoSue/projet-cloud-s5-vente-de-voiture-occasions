import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from 'react-bootstrap';
import { Modal, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const HomeComponent = () => {
    const navigate = useNavigate();
    const [listeAnnonces, setListeAnnonces] = useState([]);
    const [show, setShow] = useState(false);
    const [listeMarque, setListeMarque] = useState([]);
    const [listeModele, setListeModele] = useState([]);
    const [listeVitesse, setListeVitesse] = useState([]);
    const [listeCategorie, setListeCategorie] = useState([]);
    const [listeCarburant, setListeCarburant] = useState([]);
    const [marque, setMarque] = useState('')
    const [modele, setModele] = useState('');
    const [vitesse, setVitesse] = useState('');
    const [categorie, setCategorie] = useState('');
    const [carburant, setCarburant] = useState('');
    const [couleur, setCouleur] = useState('');
    const [prixmin, setPrixmin] = useState('');
    const [prixmax, setPrixmax] = useState('');

    useEffect(() => {
        recherche();
        getMarque();
        getVitesse();
        getCategorie();
        getCarburant();
    }, []); 

    const getCategorie = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeCategorie', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeCategorie(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getCarburant = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeCarburant', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeCarburant(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getVitesse = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeVitesse', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeVitesse(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getMarque = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeMarque', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeMarque(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getModele = async (idMarque) => {
        console.log('idMarque : '+idMarque);
        try {
            const response = await fetch(`http://localhost:8080/ListeModele?idMarque=${idMarque}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeModele(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getMore = async (annonceId) => {
        navigate(`/detailannonce/${annonceId}`)
    };

    const handleShow = () => {
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
        window.location.reload();
    }

    const recherche = async () => {
        let marque = '';
        let modeleElement = document.getElementById('modele');
        let vitesseElement = document.getElementById('vitesse');
        let categorieElement = document.getElementById('categorie');
        let carburantElement = document.getElementById('carburant');
        let couleurElement = document.getElementById('couleur');
        let prixminElement = document.getElementById('prixmin');
        let prixmaxElement = document.getElementById('prixmax');

        let modele = modeleElement ? modeleElement.value.toString() : '';
        let vitesse = vitesseElement ? vitesseElement.value.toString() : '';
        let categorie = categorieElement ? categorieElement.value.toString() : '';
        let carburant = carburantElement ? carburantElement.value.toString() : '';
        let couleur = couleurElement ? couleurElement.value.toString() : '';
        let prixmin = prixminElement ? prixminElement.value.toString() : '';
        let prixmax = prixmaxElement ? prixmaxElement.value.toString() : '';
        if(modele==='Modèle') {
            modele = '';
        }
        if(vitesse==='Boite de vitesse') {
            vitesse = '';
        }
        if(categorie==='Catégorie') {
            categorie = '';
        }
        if(carburant==='Carburant') {
            carburant = '';
        }
        const url = `http://localhost:8080/Recherche?marque=${marque}&modele=${modele}&boiteVitesse=${vitesse}&categorie=${categorie}&typeCarburant=${carburant}&couleur=${couleur}&prixMin=${prixmin}&prixMax=${prixmax}`;
        try {
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                console.log('Response bien ok!');
                const data = await response.json();
                console.log('data status : '+data.status);
                if(data.status===200) {
                    setShow(false);
                    setListeAnnonces(data.data);
                }
                else if(data.status!==200) {
                    console.log("erreur : "+data.message);
                    setShow(false);
                    setListeAnnonces(data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <div>
            <h4>Liste des annonces</h4>
            <hr></hr>
            <div className='contenu'>
                <div className='recherche'>
                    <Button variant='danger' className='boutonRecherche' onClick={handleShow}>
                        Recherche
                        <FontAwesomeIcon className='iconsearch' icon={faSearch} />
                    </Button>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Rechercher une voiture</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="mb-3">
                                <label htmlFor="username" className="form-label">Marque </label>
                                <select 
                                    id="marque"
                                    value={marque}
                                    onChange={(e) => {
                                        const selectedMarqueId = e.target.value;
                                        setMarque(selectedMarqueId);
                                        getModele(selectedMarqueId);
                                    }}
                                    className='form-select'
                                >
                                    <option hidden>Marque</option>
                                    {listeMarque.map((listemarque) => (
                                        <option key={listemarque.id} value={listemarque.id}>{listemarque.nom}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password" className="form-label">Modèle </label>
                                <select 
                                    id="modele" 
                                    value={modele} 
                                    onChange={(e) => setModele(e.target.value)}
                                    className='form-select'
                                >
                                    <option hidden>Modèle</option>
                                    {listeModele.map((listemodele) => (
                                        <option key={listemodele.id}>{listemodele.nom}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="username" className="form-label">Boite de vitesse </label>
                                <select 
                                    id="vitesse" 
                                    value={vitesse} 
                                    onChange={(e) => setVitesse(e.target.value)}
                                    className='form-select'
                                >
                                    <option hidden>Boite de vitesse</option>
                                    {listeVitesse.map((listevitesse) => (
                                        <option key={listevitesse.id}>{listevitesse.nom}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password" className="form-label">Catégorie </label>
                                <select 
                                    id="categorie" 
                                    value={categorie}
                                    onChange={(e) => setCategorie(e.target.value)}
                                    className='form-select'
                                >
                                    <option hidden>Catégorie</option>
                                    {listeCategorie.map((listecategorie) => (
                                        <option key={listecategorie.id}>{listecategorie.nom}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="username" className="form-label">Carburant </label>
                                <select 
                                    id="carburant" 
                                    value={carburant} 
                                    onChange={(e) => setCarburant(e.target.value)}
                                    className='form-select'
                                >
                                    <option hidden>Carburant</option>
                                    {listeCarburant.map((listecarburant) => (
                                        <option key={listecarburant.id}>{listecarburant.nom}</option>
                                    ))}
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password" className="form-label">Couleur </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="couleur"
                                    value={couleur}
                                    onChange={(e) => setCouleur(e.target.value)}
                                    required
                                />
                            </div>
                            <div className="mb-3 row">
                                <label htmlFor="password" className="form-label">Prix </label>
                                <div className="col">
                                    <p htmlFor="password" className="form-label">min :</p>
                                    <input
                                    type="text"
                                    className="form-control"
                                    id="prixmin"
                                    value={prixmin}
                                    onChange={(e) => setPrixmin(e.target.value)}
                                    required
                                    />
                                </div>
                                <div className="col">
                                    <p htmlFor="password" className="form-label">max :</p>
                                    <input
                                    type="text"
                                    className="form-control"
                                    id="prixmax"
                                    value={prixmax}
                                    onChange={(e) => setPrixmax(e.target.value)}
                                    required
                                    />
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" onClick={recherche}>Rechercher</Button>
                            <Button variant="secondary" onClick={handleClose}>Canncel</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
                <div className='liste'>
                    {listeAnnonces.map((annonce) => (
                    <Card className='card' key={annonce.id}>
                        <Card.Header>
                            <Card.Title>{annonce.marque} {annonce.modele} {annonce.categorie} {annonce.couleur}</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <p>Annonce fait le {annonce.dateHeure}</p>
                            <p>
                                Description : {annonce.description} ... 
                                <small><a onClick={() => getMore(annonce.id)} className='lien-voir' style={{cursor: 'pointer'}}>Voir plus</a></small>   
                            </p>
                        </Card.Body>
                    </Card>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default HomeComponent;