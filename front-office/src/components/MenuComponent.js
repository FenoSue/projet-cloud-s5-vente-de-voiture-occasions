import React from 'react';
import { Link } from 'react-router-dom';
import '../css/MenuCss.css';

const MenuComponent = (props) => {

    return (
        <div className='menu-container'>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                <Link className="navbar-brand" to="/">Site de vente</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Accueil</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/login/listeannonce">Mes annonces</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to="/login/messagerie">Contact</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/login/historique">Historique</Link>
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
            <div className="main-content">
                {props.children}
            </div>
        </div>
    );
};

export default MenuComponent;