import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from 'react-bootstrap';

const ConversationComponent = () => {
    const { annonceId } = useParams();
    const [message, setMessage] = useState([]);

    useEffect(() => {
        getMessage();
    }, []); 

    const getMessage = async () => {
        console.log('Message ok!')
        const token = localStorage.getItem('token');
        try {
            const response = await fetch(`http://localhost:8080/Discussion?idUserDestinataire=${annonceId}`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setMessage(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <div>
            <h4>Conversation</h4>
            <hr></hr>
            <div>
            {message.map((messages) => (
                <Card key={messages.id}>
                    <Card>{message.message}</Card>
                </Card>
            ))}
            </div>
        </div>
    );
};

export default ConversationComponent;