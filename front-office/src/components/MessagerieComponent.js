import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Modal, Button } from 'react-bootstrap';

const HistoriqueComponent = () => {

    useEffect(() => {
        getMessage();
    }, []); 

    const getMessage = async () => {
        console.log('Message ok!')
    };

    return (
        <div>
            <h4>Messages</h4>
            <hr></hr>
        </div>
    );
};

export default HistoriqueComponent;