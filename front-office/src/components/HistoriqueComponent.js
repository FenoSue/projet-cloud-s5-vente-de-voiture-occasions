import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const HistoriqueComponent = () => {
    const navigate = useNavigate();
    const [listeAnnonces, setListeAnnonces] = useState([]);

    useEffect(() => {
        getHistorique();
    }, []); 

    const getHistorique = async () => {
        const token = localStorage.getItem('token');
        try {
            const response = await fetch('http://localhost:8080/HistoriqueAnnonce', {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeAnnonces(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <div>
            <h4>Historique</h4>
            <hr></hr>
            <div className='contenu'>
                <div className='liste'>
                    <table className="table table-bordered custom-table">
                        <thead>
                        <tr>
                            <th>réf</th>
                            <th>Annonce</th>
                        </tr>
                        </thead>
                        <tbody>
                            {listeAnnonces.map((annonce) => (
                            <tr key={annonce.id}>
                                <td>
                                    <p>{annonce.id}</p>
                                </td>
                                <td>
                                    <p>Fait le {annonce.dateHeure}</p>
                                    <p>
                                        Description : c'est une voiture {annonce.categorie} {annonce.couleur}. {annonce.nbrPlace} {annonce.description}. 
                                        De marque {annonce.marque}, modèle {annonce.modele}. 
                                        Avec une {annonce.boiteVitesse}. Sortie en {annonce.anneeSortie}
                                    </p>
                                    <p>
                                        Carburant : {annonce.typeCarburant}
                                    </p>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default HistoriqueComponent;