import React from 'react';
import MenuComponent from '../components/MenuComponent';
import HomeComponent from '../components/HomeComponent';

const Home = () => {

    return (
        <MenuComponent>
            <HomeComponent />
        </MenuComponent>
    );
};

export default Home;