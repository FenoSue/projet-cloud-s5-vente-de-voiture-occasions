import React from 'react';
import Menu1Component from '../components/Menu1Component';
import ListeAnnonceComponent from '../components/ListeAnnonceComponent';

const ListeAnnonce = () => {

    return (
        <Menu1Component>
            <ListeAnnonceComponent />
        </Menu1Component>
    );
};

export default ListeAnnonce;