import React from 'react';
import Menu1Component from '../components/Menu1Component';
import ConversationComponent from '../components/ConversationComponent';

const Conversation = () => {

    return (
        <Menu1Component>
            <ConversationComponent />
        </Menu1Component>
    );
};

export default Conversation;