import React, { useState, useEffect } from 'react';
import '../css/HomeCss.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu1Component from '../components/Menu1Component';
import HistoriqueComponent from '../components/HistoriqueComponent';

const HomeComponent = () => {
    return (
        <Menu1Component>
            <HistoriqueComponent />
        </Menu1Component>
    );
};

export default HomeComponent