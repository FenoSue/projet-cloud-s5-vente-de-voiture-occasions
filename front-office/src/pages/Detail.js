import React from 'react';
import MenuComponent from '../components/MenuComponent';
import DetailComponent from '../components/DetailComponent';

const Detail = () => {

    return (
        <MenuComponent>
            <DetailComponent />
        </MenuComponent>
    );
};

export default Detail;