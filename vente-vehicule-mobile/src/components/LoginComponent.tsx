import React, { useState, useEffect } from 'react';
import { IonInput, IonItem, IonList, IonButton, IonRouterLink, IonLabel } from '@ionic/react';
import { useHistory } from 'react-router-dom';
import '../css/LoginCss.css';

interface ContainerProps { }

const LoginComponent: React.FC<ContainerProps> = () => {
    const history = useHistory();
    const [identifiant, setIdentifiant] = useState('');
    const [password, setPassword] = useState('');

    const connexion = async (e: React.FormEvent) => {
        console.log("connexion");
    
        console.log("identifiant : "+identifiant);
        console.log("password : "+password);
    
        e.preventDefault();
    
        try {
            const response = await fetch('http://localhost:8080/Connection', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "login":identifiant,
                    "pwd":password,
                }),
            });
    
            if (!response.ok) {
            throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
            const data = await response.json();
    
            if(data.status===200) {
                localStorage.setItem('token', data.data[0]);
                console.log("status ok");
                const url = `/home`;
                history.push(url);
            }
            else if(data.status===400) {
                console.log("erreur : "+data.data);
            }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <IonList className='ionlist'>
            <IonItem>
                <IonInput 
                    type='text' 
                    label="Entrer votre identifiant" 
                    labelPlacement="floating"
                    value={identifiant}
                    onIonChange={(e) => setIdentifiant(e.detail.value!)}
                ></IonInput>
            </IonItem>
            
            <IonItem>
                <IonInput 
                    type='password' 
                    label="Entrer votre mot de passe" 
                    labelPlacement="floating" 
                    value={password} 
                    onIonChange={(e) => setPassword(e.detail.value!)}
                ></IonInput>
            </IonItem>
            <div>
                <IonButton className='bouton' color={'primary'} onClick={connexion}>Se connecter</IonButton>
            </div>
            <div className='insription'>
                <IonLabel>Vous n'avez pas de compte?</IonLabel><strong><IonRouterLink routerLink="/inscription">Inscrivez-vous</IonRouterLink></strong>
            </div>

        </IonList>
    );
};

export default LoginComponent;