import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { IonInput, IonItem, IonList, IonButton, IonRouterLink, IonLabel } from '@ionic/react';
import '../css/LoginCss.css';

interface ContainerProps { }

const InscriptionComponent: React.FC<ContainerProps> = () => {
    const history = useHistory();
    const [identifiant, setIdentifiant] = useState('');
    const [password, setPassword] = useState('');

    const inscription = async (e: React.FormEvent) => {
        
        console.log("identifiant : "+identifiant);
        console.log("password : "+password);
    
        e.preventDefault();

        try {
            const response = await fetch('http://localhost:8080/Inscription', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "login":identifiant,
                    "pwd":password,
                }),
            });

            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();

                if(data.status===200) {
                    console.log("status ok");
                    const url = `/login`;
                    history.push(url);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    }; 

    return (
        <IonList className='ionlist'>
            <IonItem className='identifiant'>
                <IonInput 
                    type='text' 
                    label="Entrer un identifiant" 
                    labelPlacement="floating"
                    value={identifiant}
                    onIonChange={(e) => setIdentifiant(e.detail.value!)}
                ></IonInput>
            </IonItem>
            
            <IonItem className='mdp'>
                <IonInput 
                    type='password' 
                    label="Entrer un mot de passe" 
                    labelPlacement="floating"
                    value={password}
                    onIonChange={(e) => setPassword(e.detail.value!)}
                ></IonInput>
            </IonItem>
            <div>
                <IonButton onClick={inscription} className='boutoninscrit' color={'primary'}>S'inscrire</IonButton>
            </div>
            <div className='insription'>
                <IonLabel>Vous avez déjà un compte?</IonLabel><strong><IonRouterLink routerLink="/login">Connectez-vous</IonRouterLink></strong>
            </div>
        </IonList>
    );
};

export default InscriptionComponent;