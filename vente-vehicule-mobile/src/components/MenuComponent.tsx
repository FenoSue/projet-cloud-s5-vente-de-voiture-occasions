import React, { useState     } from 'react';
import { useHistory } from 'react-router-dom';
import { IonButtons, IonContent, IonHeader, IonMenu, IonMenuButton, IonPage, IonTitle, IonToolbar, IonButton, IonFooter, IonMenuToggle, IonList, IonItem,
    IonIcon, IonRouterLink } from '@ionic/react';
import { powerOutline } from 'ionicons/icons';
import '../css/MenuCss.css';

interface ContainerProps { 
    children?: React.ReactNode;
}

const MenuComponent: React.FC<ContainerProps> = (props) => {
    const history = useHistory();

    const deconnexion = async (e: React.FormEvent) => {
        console.log("deconnexion");
        e.preventDefault(); 

        try {
            const token = localStorage.getItem('token');
            const response = await fetch('http://localhost:8080/Deconnection', {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            });

            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();

                if(data.status===200) {
                    localStorage.removeItem("token");
                    console.log("status ok");
                    const url = `/login`;
                    history.push(url);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    }; 

    return (
        <>
            <IonMenu contentId="main-content">
                <IonHeader>
                    <IonToolbar>
                        <IonTitle className='iontitle'>Menu</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <IonMenuToggle autoHide={false}>
                        <IonList>
                            <IonItem>
                                <strong><IonRouterLink routerLink="/home" className='ionitem'>Accueil</IonRouterLink></strong>
                            </IonItem>
                            <IonItem className='ionitem'>
                                <strong><IonRouterLink routerLink="/ajoutannonce" className='ionitem'>Ajouter une annonce</IonRouterLink></strong>
                            </IonItem>
                            <IonItem className='ionitem'>
                                <strong><IonRouterLink routerLink="/listeannonce/valider" className='ionitem'>Liste des annonces valider</IonRouterLink></strong>
                            </IonItem>
                            <IonItem className='ionitem'>
                                <strong><IonRouterLink routerLink="/listeannonce/nonvalider" className='ionitem'>Liste des annonces non-valider</IonRouterLink></strong>
                            </IonItem>
                        </IonList>
                    </IonMenuToggle>  
                </IonContent>
                <IonFooter>
                    <IonMenuToggle autoHide={false}>
                        <IonItem className='ionitem'>
                            <IonIcon icon={powerOutline} className='icon'/>
                            <strong><IonRouterLink className='ionitem' onClick={deconnexion}>Déconnexion</IonRouterLink></strong>
                        </IonItem>
                    </IonMenuToggle>                
                </IonFooter>
            </IonMenu>
            <IonPage id="main-content">
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuToggle>
                                <IonMenuButton></IonMenuButton>
                            </IonMenuToggle>
                        </IonButtons>
                        <IonTitle>Menu</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    {props.children}
                </IonContent>
            </IonPage>
        </>
    );
};

export default MenuComponent;