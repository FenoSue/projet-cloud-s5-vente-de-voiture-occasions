import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { IonCard, IonGrid, IonList, IonCol, IonRow, IonButton } from '@ionic/react';
import '../css/ListeCss.css';
import { colorFill } from 'ionicons/icons';

interface Annonce {
    id: string;
    idproduit: string;
    idutilisateur: string;
    dateHeure: Date;
    description: string;
    statut: number;
    dateStatut: Date;
    etat: number;
    favoris: number;
    marque: string;
    modele: string;
    boiteVitesse: string;
    categorie: string;
    typeCarburant: string;
    kilometrage: number;
    anneeSortie: number;
    couleur: string;
    nbrPlace: number;
    prix: number;
}

interface ContainerProps { string: string }

const ListeAnnonceComponent: React.FC<ContainerProps> = (props) => {
    const [listeAnnonce, setListeAnnonce] = useState<Annonce[]>([]);
    const { string } = useParams<{ string: string }>();
    const [title, setTitle] = useState('');

    useEffect(() => {
        getListeAnnonce(string);
    }, []);

    const getListeAnnonce = async (string: string) => {
        const token = localStorage.getItem('token');
        let url;
        if(string==='valider') {
            url = 'ListeAnnonceValiderUser';
            setTitle('Liste des annonces valider');
        }
        else {
            url = 'ListeAnnonceNonValiderUser';
            setTitle('Liste des annonces non-valider');
        }
        try {
            const response = await fetch('http://localhost:8080/'+url, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeAnnonce(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const modifierStatut = async (idAnnonce: string) => {
        console.log('Modifier statut ok!');
        const token = localStorage.getItem('token');
        try {
            const response = await fetch('http://localhost:8080/ModifierStatutAnnonce', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: idAnnonce,
                }),
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    console.log('message : '+data.message);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    return (
        <IonList>
            <h4>{title}</h4>
            {listeAnnonce && listeAnnonce.map((annonce, index) => (
                <IonCard key={index} className='ioncardliste'>
                    <IonGrid>
                        <IonRow>
                            <IonCol>
                                <strong><h5 className='h5'>{annonce.marque} {annonce.modele} {annonce.categorie} {annonce.couleur}</h5></strong>
                                <p><strong>Boite de vitesse : </strong>{annonce.boiteVitesse}</p>
                                <p><strong>Carburant : </strong>{annonce.typeCarburant}</p>
                                <p><strong>Kilometrage : </strong>{annonce.kilometrage}</p>
                                <p><strong>Année de sortie : </strong>{annonce.anneeSortie}</p>
                                <p><strong>Prix : </strong>{annonce.prix}</p>
                            </IonCol>
                            <IonCol>
                                {annonce.statut === -1 ? (
                                    <strong><h5 style={{color: 'red'}}>Non-vendu</h5></strong>
                                ) : annonce.statut === 1 ? (
                                    <strong><h5 style={{color: 'green'}}>Vendu</h5></strong>
                                ) : null}
                            </IonCol>
                            {annonce.etat === 1 ? (
                                <IonCol>
                                    <IonButton onClick={() => modifierStatut(annonce.id)} color={'dark'}>Vendu</IonButton>
                                </IonCol>
                            ) :  null}
                        </IonRow>
                    </IonGrid>
                </IonCard>
            ))}
            
        </IonList>
    );
};

export default ListeAnnonceComponent;