import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { IonInput, IonItem, IonList, IonButton, IonCard, IonGrid, IonRow, IonCol, IonLabel, IonSelect, IonSelectOption } from '@ionic/react';
import '../css/AjoutCss.css';

interface Marque {
    id: number;
    nom: string;
}
interface Modele {
    id: number;
    idMarque: number
    nom: string;
}
interface Categorie {
    id: number;
    nom: string;
}
interface Carburant {
    id: number;
    nom: string;
}
interface Vitesse {
    id: number;
    nom: string;
}

interface ContainerProps { }

const AjoutAnnonceComponent: React.FC<ContainerProps> = () => {
    const history = useHistory();
    const [idMarque, setIdMarque] = useState('');
    const [idModele, setIdModele] = useState('');
    const [idVitesse, setIdVitesse] = useState('');
    const [idCategorie, setIdCategorie] = useState('');
    const [idCarburant, setIdCarburant] = useState('');
    const [matricule, setMatricule] = useState('');
    const [kilometrage, setKilometrage] = useState('');
    const [anneeSortie, setAnneeSortie] = useState('');
    const [couleur, setCouleur] = useState('');
    const [nbrPlace, setNbrPlace] = useState('');
    const [prix, setPrix] = useState('');
    const [description, setDescription] = useState('');

    const [selectedValue, setSelectedValue] = useState<string>('');

    const [listeMarque, setListeMarque] = useState<Marque[]>([]);
    const [listeModele, setListeModele] = useState<Modele[]>([]);
    const [listeVitesse, setListeVitesse] = useState<Vitesse[]>([]);
    const [listeCategorie, setListeCategorie] = useState<Categorie[]>([]);
    const [listeCarburant, setListeCarburant] = useState<Carburant[]>([]);

    useEffect(() => {
        getMarque();
        getVitesse();
        getCategorie();
        getCarburant();
    }, []); 

    const getMarque = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeMarque', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeMarque(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    }; 

    const getModele = async (idmarque: number) => {
        try {
            const response = await fetch(`http://localhost:8080/ListeModele?idMarque=${idmarque}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeModele(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getCategorie = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeCategorie', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeCategorie(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getCarburant = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeCarburant', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeCarburant(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const getVitesse = async () => {
        try {
            const response = await fetch('http://localhost:8080/ListeVitesse', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
        
            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();
        
                if(data.status===200) {
                    setListeVitesse(data.data);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.data);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    };

    const ajouter = async (e: React.FormEvent) => {
        e.preventDefault(); 
        const token = localStorage.getItem('token');

        console.log('token : '+token);
        console.log('idMarque : '+idMarque);
        console.log('idModele : '+idModele);
        console.log('idVitesse : '+idVitesse);
        console.log('idCategorie : '+idCategorie);
        console.log('idCarburant : '+idCarburant);
        console.log('matricule : '+matricule);
        console.log('kilometrage : '+kilometrage);
        console.log('anneeSortie : '+anneeSortie);
        console.log('couleur : '+couleur);
        console.log('nbrPlace : '+nbrPlace);
        console.log('prix : '+prix);
        console.log('description : '+description);
        console.log('Ajouter avec succès!');

        try {
            const token = localStorage.getItem('token');
            const response = await fetch(`http://localhost:8080/AjoutAnnonce?idMarque=${idMarque}&idModele=${idModele}&matricule=${matricule}&idVitesse=${idVitesse}&idCategorie=${idCategorie}&idCarburant=${idCarburant}&kilometrage=${kilometrage}&anneeSortie=${anneeSortie}&couleur=${couleur}&nbrPlace=${nbrPlace}&prix=${prix}&description=${description}`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            });

            if (!response.ok) {
                throw new Error(`Réponse du serveur non OK: ${response.status}`);
            }
            else {
                const data = await response.json();

                if(data.status===200) {
                    console.log("status ok");
                    const url = `/ajoutannonce`;
                    history.push(url);
                }
                else if(data.status===400) {
                    console.log("erreur : "+data.message);
                }
            }
        } 
        catch (error) {
            console.log("catch");
            console.error('Erreur lors de la requête HTTP :', error);
        }
    }; 

    return (
        <IonList>
            <h4>Ajouter une annonce</h4>
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <IonCard className='left'>
                            <IonItem className='identifiant'>
                                <IonLabel>Marque </IonLabel>
                                <IonSelect aria-label="Marque" value={idMarque} 
                                    onIonChange={(e) => {
                                        const selectedMarqueId = e.target.value;
                                        setIdMarque(selectedMarqueId);
                                        getModele(selectedMarqueId);
                                    }}>
                                    {listeMarque.map((listemarque, index) => (
                                        <IonSelectOption key={index} value={listemarque.id}>{listemarque.nom}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonLabel>Modele </IonLabel>
                                <IonSelect aria-label="Modele" value={idModele} onIonChange={(e) => setIdModele(e.detail.value!)}>
                                    {listeModele.map((listemodele, index) => (
                                        <IonSelectOption key={index} value={listemodele.id}>{listemodele.nom}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonLabel>Boite de vitesse </IonLabel>
                                <IonSelect aria-label="Vitesse" value={idVitesse} onIonChange={(e) => setIdVitesse(e.detail.value!)}>
                                    {listeVitesse.map((listevitesse, index) => (
                                        <IonSelectOption key={index} value={listevitesse.id}>{listevitesse.nom}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonLabel>Catégorie </IonLabel>
                                <IonSelect aria-label="Catégorie" value={idCategorie} onIonChange={(e) => setIdCategorie(e.detail.value!)}>
                                    {listeCategorie.map((listecategorie, index) => (
                                        <IonSelectOption key={index} value={listecategorie.id}>{listecategorie.nom}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonLabel>Carburant </IonLabel>
                                <IonSelect aria-label="Carburant" value={idCarburant} onIonChange={(e) => setIdCarburant(e.detail.value!)}>
                                    {listeCarburant.map((listecarburant, index) => (
                                        <IonSelectOption key={index} value={listecarburant.id}>{listecarburant.nom}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                        </IonCard>
                    </IonCol>


                    <IonCol>
                        <IonCard className='right'>
                            <IonItem className='identifiant'>
                                <IonInput 
                                    type='text' 
                                    label="Matricule" 
                                    labelPlacement="floating"
                                    value={matricule}
                                    onIonChange={(e) => setMatricule(e.detail.value!)}
                                ></IonInput>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonInput 
                                    type='text' 
                                    label="Kilométrage" 
                                    labelPlacement="floating"
                                    value={kilometrage}
                                    onIonChange={(e) => setKilometrage(e.detail.value!)}
                                ></IonInput>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonInput 
                                    type='text' 
                                    label="Année de sortie" 
                                    labelPlacement="floating"
                                    value={anneeSortie}
                                    onIonChange={(e) => setAnneeSortie(e.detail.value!)}
                                ></IonInput>
                            </IonItem>
                            <IonItem className='identifiant'>
                                <IonInput 
                                    type='text' 
                                    label="Couleur" 
                                    labelPlacement="floating"
                                    value={couleur}
                                    onIonChange={(e) => setCouleur(e.detail.value!)}
                                ></IonInput>
                            </IonItem>
                            
                            <IonItem className='mdp'>
                                <IonInput 
                                    type='text' 
                                    label="Nombre de place" 
                                    labelPlacement="floating"
                                    value={nbrPlace}
                                    onIonChange={(e) => setNbrPlace(e.detail.value!)}
                                ></IonInput>
                            </IonItem>
                        </IonCard>
                    </IonCol>
                    <IonCol></IonCol>
                </IonRow>
            </IonGrid>
            
            <IonItem className='identifiant'>
                <IonInput 
                    type='text' 
                    label="Prix" 
                    labelPlacement="floating"
                    value={prix}
                    onIonChange={(e) => setPrix(e.detail.value!)}
                ></IonInput>
            </IonItem>
            <IonItem className='mdp'>
                <IonInput 
                    type='text' 
                    label="Description" 
                    labelPlacement="floating"
                    value={description}
                    onIonChange={(e) => setDescription(e.detail.value!)}
                ></IonInput>
            </IonItem>
            
            <div>
                <IonButton onClick={ajouter} className='boutoninscrit' color={'success'}>Créer annonce</IonButton>
            </div>

        </IonList>
    );
};

export default AjoutAnnonceComponent;