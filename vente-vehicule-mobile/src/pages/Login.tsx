import React from 'react';
import { IonContent, IonPage, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import LoginComponent from '../components/LoginComponent';
import '../css/LoginCss.css';

interface ContainerProps { }

const Login: React.FC<ContainerProps> = () => {
    return (
        <IonPage className='page'>
            <center>
                <IonCard className='contenu'>
                    <IonCardHeader>
                        <IonCardTitle className='title'>Connexion</IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        <LoginComponent />
                    </IonCardContent>
                </IonCard>
            </center>
        </IonPage>
    );
};

export default Login;