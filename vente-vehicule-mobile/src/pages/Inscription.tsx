import React from 'react';
import { IonContent, IonPage, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import InscriptionComponent from '../components/InscriptionComponent';
import '../css/LoginCss.css';

interface ContainerProps { }

const Inscription: React.FC<ContainerProps> = () => {
    return (
        <IonPage className='page'>
            <center>
                <IonCard className='contenuinscrit'>
                    <IonCardHeader>
                        <IonCardTitle className='title'>Inscription</IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        <InscriptionComponent />
                    </IonCardContent>
                </IonCard>
            </center>
        </IonPage>
    );
};

export default Inscription;