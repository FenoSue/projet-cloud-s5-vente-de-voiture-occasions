import React from 'react';
import MenuComponent from '../components/MenuComponent';
import AjoutAnnonceComponent from '../components/AjoutAnnonceComponent';

interface ContainerProps { }

const AjoutAnnonce: React.FC<ContainerProps> = () => {
    return (
        <MenuComponent>
            <AjoutAnnonceComponent></AjoutAnnonceComponent>
        </MenuComponent>
    );
};

export default AjoutAnnonce;