import React from 'react';
import HomeComponent from '../components/HomeComponent';
import MenuComponent from '../components/MenuComponent';

interface ContainerProps { }

const Home: React.FC<ContainerProps> = () => {
    return (
        <MenuComponent>
            <HomeComponent></HomeComponent>
        </MenuComponent>
    );
};

export default Home;