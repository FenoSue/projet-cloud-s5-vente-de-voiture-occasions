import React from 'react';
import MenuComponent from '../components/MenuComponent';
import ListeAnnonceComponent from '../components/ListeAnnonceComponent';

interface ContainerProps { }

const ListeAnnonce: React.FC<ContainerProps> = () => {
    return (
        <MenuComponent>
            <ListeAnnonceComponent></ListeAnnonceComponent>
        </MenuComponent>
    );
};

export default ListeAnnonce;